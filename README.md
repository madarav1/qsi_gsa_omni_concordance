# qsi_GSA_Omni_concordance
This repository contains the scripts necessary to conduct a whole array concordance analysis between two genotyping arrays (e.g. 
GSA and Omni5Exome). This assumes we ran the same samples (or a subset thereof) on both arrays, as is the case for LME636X2202.
The script will go over all genotype SNPs of array 1, and array 2, determine their intersect (SNPs&Subjects), and calculate summary statistics
such as concordance over the intersecting SNPs.

